//
//  PaoNetwork.swift
//  WheatrApi
//
//  Created by PAOLA GUAMANI on 12/6/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation

class PaoNetwork {
    //completion u otro nombre, le indica q completion devolvera un string
    func getWeather(of city: String, completion:@escaping (String)->()) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&APPID=d071e3e67461c201f7d575435ddbb7fa"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){ (data, response, error)
            in
            //con guard me aseguro q exista data, y sino termina ahi ejecucion
            guard let data = data else{
                print("Error NO data")
                return
            }
            guard let weatherInfo = try? JSONDecoder().decode(PaoWheaterInfo.self, from: data) else{
                print("Error decoding weather")
                return
            }
            
            //print(weatherInfo)
            //async para q la app no se quede parada mientras se devuelve el data
            //al hilo main
            
            //para que regrese a quien lo llamó, return no permite
            //completion handler si lo permite, como el callback
            //en lugar de dispatch
            completion("\(weatherInfo.weather[0].description)")
            
            /*DispatchQueue.main.async {
                self.resultLabel.text = "\(self.cityTextField.text ?? "quito") \(weatherInfo.weather[0].description)"
                self.cityTextField.text = ""
            }*/
            
        }
        
        task.resume()
        
        
    }
}
