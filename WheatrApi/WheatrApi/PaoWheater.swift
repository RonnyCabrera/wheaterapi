//
//  PaoWheater.swift
//  WheatrApi
//
//  Created by PAOLA GUAMANI on 30/5/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation

/* json
 "mainweather":"rain"
 */
struct PaoWheaterInfo: Decodable {
    let weather:[Weather]
    //let mainweather: String
    
    /*enum CodingKeys: String, CodingKey {
        case weather
        case mainWeather = "main_weather"
    } //para cambiarle el nombre*/
}

struct Weather: Decodable {
    let id: Int
    let description: String
    
}
