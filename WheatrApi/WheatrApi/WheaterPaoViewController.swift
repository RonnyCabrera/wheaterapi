//
//  WheaterPaoViewController.swift
//  WheatrApi
//
//  Created by PAOLA GUAMANI on 29/5/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class WheaterPaoViewController: UIViewController {

    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        //
        let networking = PaoNetwork()
        networking.getWeather(of: cityTextField.text ?? "quito") { (weather) in
            DispatchQueue.main.async {
                self.resultLabel.text = weather
            }
            
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
