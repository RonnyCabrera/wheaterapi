//
//  RonnyNetwork.swift
//  WheatrApi
//
//  Created by Ronny Cabrera on 12/6/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation

class RonnyNetwork {
    func getWheater(of city: String, completion:@escaping (String)->()) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&APPID=c6df27a710c3aa09546c622f5f1e1c00"
        let urlVerdadera = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: urlVerdadera!) {
            (data, response, error) in
            
            guard let data = data else {
                print("ERROR NO DATA")
                return
            }
            guard let weatherInfoRon = try? JSONDecoder().decode(WheaterInfo.self, from: data) else {
                print("Error decoding weather ron")
                return
            }
            completion("\(weatherInfoRon.weather[0].description)")
        }
        task.resume()
    }
}
