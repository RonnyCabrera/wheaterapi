//
//  WheaterInfo.swift
//  WheatrApi
//
//  Created by Ronny Cabrera on 12/6/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation

struct WheaterInfo: Decodable {
    let weather: [WeatherRonny]
}

struct WeatherRonny: Decodable {
    let id: Int
    let description: String
    
}
