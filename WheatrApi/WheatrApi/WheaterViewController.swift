//
//  WheaterViewController.swift
//  WheatrApi
//
//  Created by Ronny Cabrera on 29/5/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class WheaterViewController: UIViewController {
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var wheaterLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SearchButtonPressed(_ sender: Any) {
        let networking = RonnyNetwork()
        networking.getWheater(of: cityTextField.text ?? "quito") {
            (wheater) in
            DispatchQueue.main.async {
                self.wheaterLabel.text = wheater
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
